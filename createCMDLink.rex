/*object for WScript.Shell will is initialized*/
ShellObjForCMD = .OLEObject~New("WScript.Shell")

Say "Creating a shortcut for CMD on your Desktop..."
DesktopVariable = ShellObjForCMD~SpecialFolders("Desktop") /*assigning a name for SpecialFolders/Desktop*/
ShortCutVariable = ShellObjForCMD~CreateShortcut(DesktopVariable || "\CMD Shortcut.lnk") /*assigning variable name for CreateShortcut*/
ShortCutVariable~TargetPath = "C:\Windows\System32\cmd.exe" /*as the name says, the target to be copied on the desktop*/
ShortCutVariable~Save /*saving the cmd.exe on the targeted path*/

ShellObjForCMD~Popup("You have now a cmd Link on your Desktop! Try!")
