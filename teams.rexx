/*declare teams */
a = "Rapid Wien"
b = "Austria Wien"
c = "Sturm Graz"
d = "Sportklub"

/*function*/

/*get info from the user*/
say "which color do you like the most: green, violett, black, white?"
pull color

SELECT
	WHEN color = green THEN SAY a;
	WHEN color = violett THEN SAY b;
	WHEN (color = black | color = white) THEN CALL bw;

	OTHERWISE SAY "please select from the given colors";
END


EXIT