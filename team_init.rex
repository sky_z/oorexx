/*output*/
t0 = .Team~New("Sturm", "Graz", "20")
SAY City t0~city Clubname t0~name Number of players t0~player
SAY "---------the team has bought a new player------------"
t0~increasePlayer /*increase the quantity of players*/
SAY "Now the team has:" t0~player "players";
/*class*/
::CLASS Team
::METHOD INIT
	EXPOSE name city player
	USE ARG name, city, player
::METHOD UNINIT
	EXPOSE name city player
	SAY "Object: <"name city player"> is about to be finalized."
::METHOD name ATTRIBUTE
::METHOD city ATTRIBUTE
::METHOD player ATTRIBUTE
::METHOD increasePlayer
	EXPOSE player /*existing players*/
	player = player + 1