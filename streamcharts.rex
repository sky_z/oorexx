/*Part 1: Internet Explorer
create an object for IE*/
myIE = .watchedIE~New("InternetExplorer.Application", "WITHEVENTS")
--set dimensions
myIE~Width = 800
myIE~Height = 600
myIE~Visible = .True
myIE~Navigate("https://spotifycharts.com/regional/at/weekly/latest/download")

call syssleep 30

/*Part 2: Excel

create an wscript object for special folder downloads*/
WshShellObj = .OLEObject~New("WScript.Shell")
Desktop = WshShellObj~SpecialFolders("Desktop")

--reading from downloaded csv file
file =.stream~new(Desktop || "\regional-at-weekly-latest.csv")

--creating new Excel Object
xlObject = .OLEObject~new("Excel.Application")
Woorksheet = xlObject~Workbooks~Add~Worksheets[1]
Active=xlObject~ActiveSheet
xlObject~Visible = .true

--reading file data and saving it into excel
i=1
pattern1 = ","
do while file~lines<>150
	text=file~linein 
	PARSE VAR text position (pattern1) trackname (pattern1) artist (pattern1) streams (pattern1) url 
	--SAY position "-" trackname "-" artist "-" streams "-" url
	Active~Range("A" || i)~Value = position
	Active~Range("B" || i)~Value = trackname
	Active~Range("C" || i)~Value = artist
	Active~Range("D" || i)~Value = streams
	Active~Range("E" || i)~Value = url
	i = i+1
end

--delete first line and row, not needed
Active~Rows("1:1")~DELETE
--saving file to desktop
xlObject~ActiveWorkbook~SaveAs(Desktop || "\streamcharts.xls")


--Part 3: Spotify
WshShellObj~Run("spotify:playlist:37i9dQZEVXbKNHh6NIXu36")
SAY "Enjoy listening to the current charts!"

exit

::CLASS watchedIE SUBCLASS OLEObject

::METHOD FileDownload
SAY "Please download the file from the page, to your Desktop"
