/*create instances of class*/
t1 = .TEAM~NEW; 
t1~name="Rapid"; t1~city="Vienna"; t1~player="23";
t2 = .TEAM~NEW; 
t2~name="Austria"; t2~city="Vienna"; t2~player="22";

/*output*/
SAY t1~city t1~name t1~player
SAY t2~city t2~name t2~player

/*class*/
::CLASS Team
::METHOD name ATTRIBUTE
::METHOD city ATTRIBUTE
::METHOD player ATTRIBUTE
::METHOD increasePlayer
	EXPOSE player /*existing players*/
	USE ARG increase
	player = player + increase